<?php
$I = new FunctionalTester($scenario);
$I->wantTo('Set a Custom Number of Posts And See the Updated Setting');

$plugin_id = 'recent-posts-plugin';
$new_setting_value = '7';
$I->loginAsAdmin();
$I->amOnPluginsPage();
$I->activatePlugin($plugin_id);
$I->click('recent-posts-options');
$I->fillField(['id'=>'plugin_text_string'], $new_setting_value);
$I->click('Save Changes');
$I->seeInDatabase('wp_options', ['option_name' => 'recent-posts-options', 'option_value' => $new_setting_value]);
