<?php
$plugin_id = 'recent-posts-plugin';

$I = new FunctionalTester($scenario);
$I->wantTo('Activate the Plugin and see the dummy Post being Inserted Into DB');
$I->loginAsAdmin();
$I->amOnPluginsPage();
$I->deactivatePlugin($plugin_id);
$I->activatePlugin($plugin_id);
$I->seeInDatabase('wp_posts', ['post_title' => 'Inserted on plugin activation']);
