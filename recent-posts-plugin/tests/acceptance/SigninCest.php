<?php 

class SigninCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    // tests
    public function tryToTest(AcceptanceTester $I)
    {
		$I->loginAsAdmin();
		$I->amOnPluginsPage();
		$I->see('Howdy, Pegasus Uganda');
    }
}
