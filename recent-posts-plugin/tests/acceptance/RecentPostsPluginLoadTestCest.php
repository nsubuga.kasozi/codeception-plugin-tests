<?php

class RecentPostsPluginLoadTestCest
{
  public function _before(AcceptanceTester $I)
  { }

  // tests
  public function tryToTest(AcceptanceTester $I)
  {
    $plugin_id = "recent-posts-plugin";

    
    $I->loginAsAdmin();
    $I->amOnPluginsPage();
    $I->activatePlugin($plugin_id);
    $I->click($plugin_id);
    $I->see('Recent Posts Settings');
  }
}
