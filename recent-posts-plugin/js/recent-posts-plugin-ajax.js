jQuery(document).ready(function ($) {

    "use strict";
    /**
         * The file is enqueued from inc/admin/class-admin.php.
     */
    $('#recent-posts-form').submit(function (event) {

        var input = $('#input-div').html();
        event.preventDefault(); // Prevent the default form submit.            

        // serialize the form data
        var ajax_form_data = $("#recent-posts-form").serialize();

        //add our own ajax check as X-Requested-With is not always reliable
        ajax_form_data = ajax_form_data + '&ajaxrequest=true&submit=Submit+Form';

        $.ajax({
            url: params.ajaxurl, // domain/wp-admin/admin-ajax.php
            type: 'post',
            data: ajax_form_data
        })

            .done(function (response) { // response from the PHP action
                if (response.success) {
                    window.location.reload();
                    $("#form_submit_feedback ").html("<h2>The request was successful</h2>");
                }
                else {
                    $("#form_submit_feedback ").html(response.data);
                }
            })

            // something went wrong  
            .fail(function (response) {
                $("#form_submit_feedback ").html("<h2>Something went wrong.</h2>");
            })

            // after all this time?
            .always(function () {
                //event.target.reset();
            });

    });

});