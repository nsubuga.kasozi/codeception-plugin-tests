<?php
/**
 * recent-posts Plugin is the simplest WordPress plugin for beginner.
 * Take this as a base plugin and modify as per your need.
 *
 * @package recent-posts Plugin
 * @author recent-posts
 * @license GPL-2.0+
 * @link https://recent-posts.com/tag/wordpress-beginner/
 * @copyright 2017 Kasozi, LLC. All rights reserved.
 *
 *            @wordpress-plugin
 *            Plugin Name: recent-posts Plugin
 *            Plugin URI: https://recent-posts.com/tag/wordpress-beginner/
 *            Description: recent-posts Plugin
 *            Version: 3.0
 *            Author: Kasozi
 *            Author URI: https://recent-posts.com/
 *            Text Domain: recent-posts
 *            Contributors: recent-posts
 *            License: GPL-2.0+
 *            License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

// @COMMENT: This header could have just been the last part after the '@copyright' line . Something like this:
// *            Plugin Name: Recent Posts Plugin
// *            Plugin URI: https://kanzucode.com
// *            Description: Display recent posts
// *            Version: 3.0
// *            Author: Kasozi
// *            Author URI: https://kanzucode.com
// *            Text Domain: recent-posts
// *            Contributors: recent-posts
// *            License: GPL-2.0+
// *            License URI: http://www.gnu.org/licenses/gpl-2.0.txt
if (!defined('ABSPATH')) {
    die;
}

// @COMMENT: WP generally uses underscores when naming so this would be Recent_Posts. Adding 'Plugin' to the class name isn't necessary
class Recent_Posts
{

    // @COMMENT: WordPress generally uses snake_case in naming. See https://make.wordpress.org/core/handbook/best-practices/coding-standards/php/
    // @COMMENT: Update these to use snake_case
    public $settings_options_group = 'recent-posts-options';
    public $setting_options_name = 'recent-posts-options';
    public $default_number_of_posts = 3;
    public $Version = "3";


    public function register_plugin()
    {
        //add a top level menu item for the plugin on the dashboard
        add_action('admin_menu', array($this, 'add_plugin_to_admin_menu'));
        add_action('admin_init', array($this, 'register_plugin_initial_settings'));
        add_action('admin_post_custom_action_hook', array($this, 'save_updated_setting_callback'));
        add_action('wp_ajax_custom_action_hook', array($this, 'save_updated_setting_callback_ajax'));
        add_action('admin_enqueue_scripts', array($this, 'enqueue_javascript_files_necessary_for_plugin'));
        return true;
    }

    public function save_updated_setting_callback_ajax()
    {
        $setting_group = $this->settings_options_group;

        $saved_result = "Failed";

        //only save if the key exists in the POST variables
        if (isset($_POST[$setting_group])) {
            $saved_result = $this->save_setting_value();
        }


        return $saved_result == "OK" ?
            wp_send_json_success() : wp_send_json_error($saved_result);
    }

    public function save_updated_setting_callback()
    {

        $setting_group = $this->settings_options_group;

        $saved_result = "Failed";

        //only save if the key exists in the POST variables
        if (isset($_POST[$setting_group])) {
            $saved_result = $this->save_setting_value();
        }

        //redirect user back
        $redirect_url = 'Location: ' . admin_url('admin.php') . '?page=recent-posts-options';
        wp_redirect($redirect_url);
        exit();
    }

    public function add_plugin_to_admin_menu()
    {
        $name = $this->settings_options_group;
        add_menu_page($name, $name, 'manage_options', $name, array($this, 'get_the_html_for_the_custom_options_page'));
    }

    public function register_plugin_initial_settings()
    {

        //create settings in the settings table
        //register_setting($this->settings_options_group, $this->setting_options_name);

        add_settings_section('plugin_main', 'Main Settings', array($this, 'get_label_for_input_text_box'), $this->setting_options_name);

        //html display and to recieve the updated setting
        add_settings_field('plugin_text_string', 'Number of Posts To Display', array($this, 'get_html_for_input_text_box_of_setting'), $this->setting_options_name, 'plugin_main');
    }

    public function enqueue_javascript_files_necessary_for_plugin()
    {
        $params = array('ajaxurl' => admin_url('admin-ajax.php'));
        wp_enqueue_script('nds_ajax_handle', plugin_dir_url(__FILE__) . 'js/recent-posts-plugin-ajax.js', array('jquery'), $this->Version, false);
        wp_localize_script('nds_ajax_handle', 'params', $params);
    }

    public function get_label_for_input_text_box()
    {
        echo "<p>Number of Posts To Display</p>";
    }

    public function get_html_for_input_text_box_of_setting()
    {
        $setting_group = $this->settings_options_group;
        echo "<label>Enter Num:</label> <input id='plugin_text_string' name='$setting_group' size='40' type='text' value='" . $this->get_number_of_recent_posts() . "' />";
    }

    // generate the html form
    public function get_the_html_for_the_custom_options_page()
    {
        echo ('<div class="wrap">');

        echo ('<h1>Recent Posts Settings</h1>');

        //check if there is an error message
        echo ('<div id="form_submit_feedback">' . (isset($_GET['Msg']) ? $_GET['Msg'] : '') . '</div>');

        echo ('<form id="recent-posts-form" method="post" action="' . admin_url('admin-post.php') . '?page=recent-posts-options"> ');
        echo "<input type='hidden' name='action' value='custom_action_hook' />";
        echo "<p>Specify the Number of Posts To Display</p>";
        echo "<div id='input-div'>";
        $this->get_html_for_input_text_box_of_setting();
        echo "</div>";
        submit_button();
        echo ('</form>');

        echo ('<h1>Your Recent Posts</h1>');
        echo ('<div id="recent-posts-list">');
        $this->get_recent_posts();
        echo ('</div>');

        echo ('</div>');
    }

    // get the most recent posts and echo them out
    public function get_recent_posts()
    {
        $numberOfPosts = $this->get_number_of_recent_posts();

        $args = array(
            'numberposts' => $numberOfPosts,
            'offset' => 0,
            'category' => 0,
            'orderby' => 'post_date',
            'order' => 'DESC',
            'include' => '',
            'exclude' => '',
            'meta_key' => '',
            'meta_value' => '',
            'post_type' => 'post',
            'post_status' => 'draft, publish, future, pending, private',
            'suppress_filters' => true,
        );

        $recent_posts = wp_get_recent_posts($args);

        foreach ($recent_posts as $recent) {
            echo '<li><a href="' . get_permalink($recent['ID']) . '">' . $recent['post_title'] . '</a> </li> ';
        }

        wp_reset_query();
    }

    public function save_setting_value()
    {

        // get whatever he has input
        $setting_group = $this->settings_options_group;

        // @TODO Look at WordPress security guidelines, particularly, securing user input https://developer.wordpress.org/plugins/security/securing-input/
        $new_value_supplied = sanitize_text_field($_POST[$setting_group]);

        // validate that its an int
        if (!is_numeric($new_value_supplied)) {
            return ("<h2>Setting of [$new_value_supplied] is not an INT</h2>");;
        }

        // update
        $numberOfPostsPerPage = $new_value_supplied;
        $this->update_the_number_of_posts_per_page($numberOfPostsPerPage);
        return "OK";
    }

    // update setting value
    // @TODO All these methods should be named following snake_case
    public function update_the_number_of_posts_per_page(int $numberOfPosts)
    {
        update_option($this->settings_options_group, $numberOfPosts);
        return true;
    }

    // get the current setting value
    public function get_number_of_recent_posts()
    {
        return get_option($this->settings_options_group);
    }

    // insert a post on activation
    public function activate_plugin()
    {
        $this->update_the_number_of_posts_per_page($this->default_number_of_posts);
        $this->save_initial_post_on_plugin_activation();
        flush_rewrite_rules();
    }

    public function save_initial_post_on_plugin_activation()
    {

        // Create post object
        $initial_post = array(
            'post_title'    => 'Inserted on plugin activation',
            'post_content'  => 'Recent Posts Plugin Dummy Post Content',
            'post_status'   => 'publish',
            'post_author'   => 1,
            'post_category' => array(8, 39), // @TODO How sure are you these categories exist? You'd need to first create them
            'ID' => 0, // to ensure insertion
        );

        // Insert the post into the database
        wp_insert_post($initial_post);
    }

    public function deactivate_plugin()
    {
        flush_rewrite_rules();
    }

    public function uninstall_plugin()
    { }
}

// initialize class
if (class_exists('Recent_Posts')) {
    $recent_posts_plugin = new Recent_Posts();
    $recent_posts_plugin->register_plugin();
}

// writes stuff out to debug.log
//$log - string or array or object you want to dumpt out to log file
function write_to_log($log)
{
    if (is_array($log) || is_object($log)) {
        error_log(print_r($log, true));
    } else {
        error_log($log);
    }
}

// activation
register_activation_hook(__FILE__, array($recent_posts_plugin, 'activate_plugin'));

// deactivation
register_deactivation_hook(__FILE__, array($recent_posts_plugin, 'deactivate_plugin'));
